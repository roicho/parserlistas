import re
from shutil import copyfile
from datetime import datetime

def update_file(ruta, input_list):
    with open(ruta, 'w') as file:
        output_list = ''.join(input_list)
        file.write(output_list)

def get_file_info(ruta):
    with open(ruta, 'r') as file:
        # get all file content into a variable
        allLines = file.read().split('\n')
        return allLines

def fill_list(input_list):
    for i in range(len(input_list)):
        if (len(input_list[i]) > 0):
            if (input_list[i][len(input_list[i])-1] != '\n'):
                input_list[i] = input_list[i] + ',' + '\n'


copyfile('ListasSan-Larga.csv', 'BACKUP_ListasSan-Larga.csv')
copyfile('ListasSan-Corta.csv', 'BACKUP_ListasSan-Corta.csv')


listas_larga = get_file_info('ListasSan-Larga.csv')
listas_corta = get_file_info('ListasSan-Corta.csv')

now = datetime.now()
now = now.strftime("%d/%m/%Y")

listas_larga[0] = listas_larga[0] + ',' + now + '\n'
listas_corta[0] = listas_corta[0] + ',' + now + '\n'

f_input = open('Input.csv', 'r')
for line_input in f_input:
    line_input = re.sub(r'[^\x00-\x7F]+','',line_input).decode('utf-8','ignore').strip()
    line_input = line_input.split(',')

    
    id_usuario = int(line_input[0])
    pos_larga = line_input[1]
    pos_corta = line_input[2]
    situacion = line_input[3]
    if situacion == '1':
        pos_corta = 'S'
        pos_larga = 'S'
    elif situacion == '2':
        pos_corta = 'M'
    listas_larga[id_usuario] = listas_larga[id_usuario] + ',' + pos_larga + '\n'
    listas_corta[id_usuario] = listas_corta[id_usuario] + ',' + pos_corta + '\n'
f_input.close()

fill_list(listas_larga)
fill_list(listas_corta)

update_file('ListasSan-Larga.csv', listas_larga)
update_file('ListasSan-Corta.csv', listas_corta)